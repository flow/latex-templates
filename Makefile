.PHONY: all main clean distclean

LMK_FLAGS := -e '$$pdflatex=q/pdflatex %O -shell-escape %S/' -pdf -g
LATEXMK := latexmk
MAIN := main
MAINTEX := $(MAIN).tex
PDFOUT := $(MAIN).pdf
DEPS := $(MAINTEX)

all: main

main: $(PDFOUT)

%.pdf: $(DEPS)
	$(LATEXMK) $(LMK_FLAGS) $(MAINTEX)

clean:
	$(LATEXMK) -c -f

distclean: clean
	rm -f $(PDFOUT)
